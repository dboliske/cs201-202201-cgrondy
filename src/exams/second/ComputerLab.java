package exams.second;

public class ComputerLab extends Classroom {
	
	private boolean computers;
	
	public ComputerLab() {
		super();
		computers = true;
	}

	public boolean hasComputers() {
		return computers;
	}

	public void setComputers(boolean computers) {
		this.computers = computers;
	}
	
	@Override
	public String toString() {
		return super.toString() + "Has Computers: " + computers + ".";
	}

}
