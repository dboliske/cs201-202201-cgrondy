package exams.second;

public class Circle extends Polygon {
	
	private double radius;
	
	public Circle() {
		this.radius = 1.0;
	}
	
	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		if (radius > 0) {
			this.radius = radius;
		}
	}

	@Override
	public String toString() {
		return super.toString() + "Radius: " + radius + ". Area: " + area() + ". Perimeter: " + perimeter() + ".";
	}
	
	@Override
	public double area() {
		return Math.PI * radius * radius;
	}
	
	@Override
	public double perimeter() {
		return 2.0 * Math.PI * radius;
	}

}
