package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

public class QuestionThree {
	
	public static ArrayList<Double> sort(ArrayList<Double> list) {
		boolean done = false;
		
		do {
			done = true;
			for (int i=0; i<list.size() - 1; i++) {
				if (list.get(i+1).compareTo(list.get(i)) < 0) {
					double temp = list.get(i+1);
					list.set(i+1, list.get(i));
					list.set(i, temp);
					done = false;
				}
			}
		} while (!done);
		
		return list;
	}

	public static void main(String[] args) {
		ArrayList<Double> list = new ArrayList<Double>();
		Scanner input = new Scanner(System.in);
		boolean done = false;
		do {
			System.out.println("Type 'Enter' to enter a number.");
			System.out.println("Type 'Done' when you're finished entering numbers");
			
			System.out.print("Choice: "); // choose between entering input and moving on
			String choice = input.nextLine();
			switch (choice) {
			case "Enter": // enter a number
				System.out.print("Enter a number: ");
				double num = Double.parseDouble(input.nextLine());
				list.add(num);
				break;
			case "Done": // done entering numbers
				done = true;
				break;
			default:
				System.out.println("Invalid input.  Input is CaSe SeNsItIvE.");
			}
		} while (!done);
		
		list = sort(list);
		System.out.println("Minimum: " + list.get(0) + ". Maximum: " + list.get(list.size()-1));
		
		input.close();

	}

}
