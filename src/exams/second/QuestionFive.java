package exams.second;

import java.util.Scanner;

public class QuestionFive {

	public static int search(Double[] num, Double value) {
		int step = (int)Math.sqrt(num.length);
		int prev = 0;
		while (num[Math.min(step, num.length -1)] < value) {
			prev = step;
			step += (int)Math.sqrt(num.length);
			if (prev >= num.length) {
				return -1;
			}
		}
		
		while (num[prev] < value) {
			prev++;
			if (prev == Math.min(step,  num.length)) {
				return -1;
			}
		}
		
		if (num[prev].doubleValue() == value.doubleValue()) {
			return prev;
		}
		
		return -1;
	}

	public static void main(String[] args) {
		Double[] num = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};

		Scanner input = new Scanner(System.in);
		System.out.print("Search term: ");
		Double value = Double.parseDouble(input.nextLine());
		int index = search(num, value);
		if (index == -1) {
			System.out.println(index);
		} else {
			System.out.println(value + " found at index " + index + ".");
		}
		
		input.close();
	}

}
