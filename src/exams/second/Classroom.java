package exams.second;

public class Classroom {
	
	protected String building;
	protected String roomNumber;
	private int seats;
	
	public Classroom() {
		this.building = "Main";
		this.roomNumber = "101";
		this.seats = 1;
	}

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public int getSeats() {
		
		return seats;
	}

	public void setSeats(int seats) {
		if (seats > 0) {
			this.seats = seats;
		}
	}
	
	@Override
	public String toString() {
		return "Building Name: " + building + ". Room Number: " + roomNumber + ". Seats: " + seats + ". ";
	}

}
