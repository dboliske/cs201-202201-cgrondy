package exams.second;

public abstract class Polygon {
	
	protected String name;
	
	public Polygon() {
		this.name = "Rectangle";
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Name: " + name + ". ";
	}

	public abstract double area();
	public abstract double perimeter();

}
