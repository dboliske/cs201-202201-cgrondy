package exams.first;

import java.util.Scanner;

public class QuestionTwo {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // Creates scanner
		
		System.out.print("Enter an integer: ");
		int x = Integer.parseInt(input.nextLine()); // Stores input
		
		if (x%2 == 0 && x%2 != 0) {
			System.out.print("Foo");
		} else 	if (x%3 == 0 && x%2 != 0) {
			System.out.print("Bar");
		} else if (x%2 == 0 && x%3 == 0) {
			System.out.print("Foobar");
		}
		
		input.close();
	}

}
