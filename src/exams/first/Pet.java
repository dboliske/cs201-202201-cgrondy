package exams.first;

public class Pet {
	
	private String name;
	private int age;
	
	public Pet() {
		name = "Dog";
		age = 1;
	}
	
	public Pet(String name, int age) {
		this.name = name;
		this.age = 1;
		setAge(age);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAge(int age) {
		if (age >= 0) {
			this.age = age;
		}
	}
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public boolean equals (Pet a1) {
		if (this.name != a1.getName()) {
			return false;
		} else if (this.age != a1.getAge()) {
			return false;
		}
		
		return true;
	}
	
	public String toString() {
		return "Name: " + name + "; Age: " + age;
	}
	
}
