package exams.first;

import java.util.Scanner;

public class QuestionThree {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // read user input
		
		System.out.print("Size: ");
		int size = Integer.parseInt(input.nextLine()); // input is the chosen dimension of the square
		
		int x = 0;
		int y = 0; // initialize 
		
		while (x < size) {
			while (y < (size-x)) {
				y++;
				System.out.print("* "); // fills out the row
			}
			x++;
			System.out.println(); // moves to next row once the current one is filled
			y = 0; // resets y so it fills in the next row
		}

		input.close();
	}

}
