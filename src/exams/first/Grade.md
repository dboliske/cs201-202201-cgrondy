# Midterm Exam

## Total

90/100

## Break Down

1. Data Types:                  17/20
    - Compiles:                 3/5
    - Input:                    5/5
    - Data Types:               4/5
    - Results:                  5/5
2. Selection:                   19/20
    - Compiles:                 5/5
    - Selection Structure:      9/10
    - Results:                  5/5
3. Repetition:                  19/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               4/5
4. Arrays:                      16/20
    - Compiles:                 4/5
    - Array:                    5/5
    - Exit:                     5/5
    - Results:                  2/5
5. Objects:                     19/20
    - Variables:                5/5
    - Constructors:             5/5
    - Accessors and Mutators:   5/5
    - toString and equals:      4/5

## Comments

1. Compiler issues and integer is not cast to character correctly.
2. Nearly perfect, just a type in the first condition.
3. Good, but prints upside down.
4. Doesn't correctly compare all of the values and doesn't print repeated values.
5. Nearly perfct, but the `equals` method doesn't follow the UML diagram and take an Object as a parameter.
