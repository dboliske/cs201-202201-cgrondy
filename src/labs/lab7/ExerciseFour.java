package labs.lab7;

import java.util.Scanner;

public class ExerciseFour {

	public static int search(String[] array, String value) {
		return search(array, 0, array.length - 1, value);
	}
	
	public static int search(String[] array, int start, int end, String value) {
		int middle = (start + end) / 2;
		
		if (start > end) {
			return -1;
		}
		
		if (array[middle].equalsIgnoreCase(value)) {
			return middle;
		} else if (array[middle].compareToIgnoreCase(value) < 0) {
			return search(array, middle + 1, end, value);
		} else {
			return search(array, start, middle - 1, value);
		}
		
	}

	public static void main(String[] args) {
		String[] lang = {"c", "html", "java", "python", "ruby", "scala"};

		Scanner input = new Scanner(System.in);
		System.out.print("Search term: ");
		String value = input.nextLine();
		int index = search(lang, value);
		if (index == -1) {
			System.out.println(value + " not found.");
		} else {
			System.out.println(value + " found at index " + index + ".");
		}
		
		input.close();
	}

}
