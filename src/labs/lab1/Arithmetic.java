package labs.lab1;

import java.util.Scanner;

public class Arithmetic {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // Creates scanner
		
		System.out.print("Enter your age: ");
		double x = Double.parseDouble(input.nextLine()); // Stores input
		
		System.out.print("Enter your fathers age: ");
		double y = Double.parseDouble(input.nextLine()); // Stores input
		
		// subtracts your age from your fathers age
		System.out.println("Your father is " + (y-x) + " years older than you.");
		
		System.out.print("Enter your birth year: ");
		double a = Double.parseDouble(input.nextLine()); // Stores input
		
		// Doubles the entered birth year and displays it
		System.out.println("Your birth year doubled is " + (a*2));
		
		System.out.print("Enter your height in inches: ");
		double b = Double.parseDouble(input.nextLine()); // Stores input
		
		// multiplies the input height by 2.54 to convert it to centimeters
		System.out.println("Your height in centimeters is " + (b*2.54));
		
		// type casts inches to feet with int to remove unused decimal, then uses % to find the leftover inches and displays them
		System.out.println("Your height in feet/inches is " + ((int)(b/12)) + " feet and " + ((int)(b%12)) + " inches.");
		
		input.close(); // Close scanner

	}

}
