package labs.lab1;

import java.util.Scanner;

public class CharInput {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // Creates scanner
		
		System.out.print("Enter your first name: ");
		String name = input.nextLine(); // stores input to be used later
		
		char initial = name.charAt(0);  // charAt(0) reads first letter of input stored by String name, char initial stores that letter
		
		System.out.println("Your first initial is " + initial); // displays char initial
		
		input.close(); // Close scanner

	}

}
