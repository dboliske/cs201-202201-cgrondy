package labs.lab3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class ExerciseTwo {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		boolean done = false;
		int[] number = new int[1];
		int count = 0; // declare non-loop variables

		
		do {
			System.out.println("Type 'Enter' to enter a number.");
			System.out.println("Type 'Done' when you're finished entering numbers");
			
			System.out.print("Choice: "); // choose between entering input and moving on
			String choice = input.nextLine();
			switch (choice) {
			case "Enter": // enter a number
				if (count == number.length) { // increase array size if necessary
					int[] bigger = new int[2 * number.length];
					for (int i=0; i<number.length; i++) {
						bigger[i] = number[i];
					}
					number = bigger;
					bigger = null;
				}
				try {
				System.out.print("Enter a number: ");
				number[count] = Integer.parseInt(input.nextLine()); // store array input
				count++;
				} catch (Exception x){
					System.out.println("Invalid input.  Please try again.");
				}
				break;
			case "Done": // done entering numbers
				done = true;
				break;
			default:
				System.out.println("Invalid input.  Input is CaSe SeNsItIvE.");
			}
		} while (!done);
		
		int[] smaller = new int[count]; // removes array parts not being used
		for (int i=0; i<count; i++) {
			smaller[i] = number[i];
		}
		number = smaller;
		smaller = null;
		
		System.out.print("Enter the name you want for the file: ");
		String filename = input.nextLine(); // stores name of file
		try {
			FileWriter f = new FileWriter("src/labs/lab3/" + filename + ".txt"); // creates file
			for (int i=0; i<number.length; i++) {
				f.write(number[i] + "\n");
			}
			f.flush(); 
			f.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		input.close();
	}

}
