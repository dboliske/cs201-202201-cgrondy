package labs.lab4;

public class PhoneNumberApplication {

	public static void main(String[] args) {
		PhoneNumber a1 = new PhoneNumber(); // default constructor
		
		String PN1 = a1.toString(); // splits toString values into array
		String[] num1 = PN1.split("-");
		
		System.out.println("Country Code: " + num1[0] + "; Area Code: " + num1[1] + "; Number: " + num1[2]);
		
		PhoneNumber a2 = new PhoneNumber("12", "123", "1234567"); // non-default constructor
		
		String PN2 = a2.toString(); // splits toString values into array
		String[] num2 = PN2.split("-");
		
		System.out.println("Country Code: " + num2[0] + "; Area Code: " + num2[1] + "; Number: " + num2[2]);
		

	}

}
