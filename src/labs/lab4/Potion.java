package labs.lab4;

public class Potion {

	private String name; // Step 1 requirement
	private double strength;
	
	public Potion() { // Step 2 requirement
		name = "Health";
		strength = 1;
	}
	
	public Potion(String name, double strength) { // Step 3 requirement
		this.name = name;
		this.strength = 0;
		setStrength(strength);
	}
	
	public String getName() { // Step 4 partial requirement
		return name;
	}
	
	public double getStrength() { // Step 4 partial requirement
		return strength;
	}
	
	public void setName(String name) { // Step 5 partial requirement
		this.name = name;
	}
	
	public void setStrength(double strength) { // Step 5 partial requirement
		if (strength >= 0 && strength <=10) {
			this.strength = strength;
		}
	}
	
	public String toString() { // Step 6 requirement
		return name + " Potion: Strength " + strength;
	}
	
	public boolean validStrength(double strength) { // Step 7 requirement
		if (strength < 0 || strength > 10) {
			return false;
		}
		
		return true; // boolean cannot be conditional, so it has to be declared outside if
	}
	
	public boolean equals(Potion p) { // Step 8 requirement
		if (this.name != p.getName()) {
			return false;
		} else if (this.strength != p.getStrength()) {
			return false;
		}
		
		return true; // boolean cannot be conditional, so it has to be declared outside if
	}
}
