package labs.lab4;

public class PotionApplication {

	public static void main(String[] args) {
		Potion a1 = new Potion(); // default constructor
		
		String P1 = a1.toString(); // splits toString values into array
		String[] num1 = P1.split(" Potion: Strength ");
		
		System.out.println("Potion Name: " + num1[0] + "; Potion Strength: " + num1[1]);
		
		Potion a2 = new Potion("Mana", 2); // non-default constructor
		
		String P2 = a2.toString(); // splits toString values into array
		String[] num2 = P2.split(" Potion: Strength ");
		
		System.out.println("Potion Name: " + num2[0] + "; Potion Strength: " + num2[1]);
	}

}
