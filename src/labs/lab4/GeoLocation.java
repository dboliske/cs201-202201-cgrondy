package labs.lab4;

public class GeoLocation { // Step 1 requirement
	private double lat;
	private double lng;
	
	public GeoLocation() { // Step 2 requirement
		lat = 0.0;
		lng = 0.0;
	}
	
	public GeoLocation(double lat, double lng) { // Step 3 requirement
		this.lat = 0.0;
		setLat(lat);
		this.lng = 0.0;
		setLng(lng);
	}
	
	public double getLat() { // Step 4 partial requirement
		return lat;
	}
	
	public double getLng() { // Step 4 partial requirement
		return lng;
	}
	
	public void setLat(double lat) { // Step 5 partial requirement
		if (lat >= -90 && lat <=90) {
			this.lat = lat;
		}
	}
	
	public void setLng(double lng) { // Step 5 partial requirement
		if (lng >= -180 && lng <=180) {
			this.lng = lng;
		}
	}
	
	public String toString() { // Step 6 requirement
		return "(" + lat + ", " + lng + ")";
	}
	
	public boolean validLat(double lat) { // Step 7 requirement
		if (lat < -90 || lat > 90) {
			return false;
		}
		
		return true; // boolean cannot be conditional, so it has to be declared outside if
	}
	
	public boolean validLng(double lng) { // Step 8 requirement
		if (lng < -180 || lng > 180) {
			return false;
		}
		
		return true;
	}
	
	public boolean equals(GeoLocation g) { // Step 9 requirement
		if (this.lat != g.getLat()) {
			return false;
		} else if (this.lng != g.getLng()) {
			return false;
		}
		
		return true;
	}
}
