package labs.lab4;

public class GeoLocationApplication {

	
	public static void main(String[] args) {
		GeoLocation a1 = new GeoLocation(); // default constructor
		System.out.println("Lattitude: " + a1.getLat());
		System.out.println("Longitude: " + a1.getLng());
		
		GeoLocation a2 = new GeoLocation(90, -5); // non-default constructor
		System.out.println("Lattitude: " + a2.getLat());
		System.out.println("Longitude: " + a2.getLng());

		
	}

}
