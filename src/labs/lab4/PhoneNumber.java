package labs.lab4;

public class PhoneNumber {
	
	private String countryCode; // Step 1 requirement
	private String areaCode;
	private String number;
	
	public PhoneNumber() { // Step 2 requirement
		countryCode = "1";
		areaCode = "100";
		number = "1000000";
	}
	
	public PhoneNumber(String cCode, String aCode, String number) { // Step 3 requirement
		countryCode = cCode;
		areaCode = "100";
		setAreaCode(aCode);
		this.number = "1000000";
		setNumber(number);
	}
	
	public String getCountryCode() { // Step 4 partial requirement
		return countryCode;
	}
	
	public String getAreaCode() { // Step 4 partial requirement
		return areaCode;
	}
	
	public String getNumber() { // Step 4 partial requirement
		return number;
	}
	
	public void setCountryCode(String cCode) {  // Step 5 partial requirement
		countryCode = cCode;
	}
	
	public void setAreaCode(String aCode) {  // Step 5 partial requirement
		if (aCode.length() == 3) {
			areaCode = aCode;
		}
	}
	
	public void setNumber(String number) {  // Step 5 partial requirement
		if (number.length() == 7) {
			this.number = number;
		}
	}
	
	public String toString() { // Step 6 requirement
		return countryCode + "-" + areaCode + "-" + number;
	}
	
	public boolean validAreaCode(String aCode) { // Step 7 requirement
		if (aCode.length() != 3) {
			return false;
		}
		
		return true; // boolean cannot be conditional, so it has to be declared outside if
	}
	
	public boolean validNumber(String number) { // Step 8 requirement
		if (number.length() != 7) {
			return false;
		}
		
		return true; // boolean cannot be conditional, so it has to be declared outside if
	}
	
	public boolean equals(PhoneNumber pn) { // Step 9 requirement
		if (this.countryCode != getCountryCode()) {
			return false;
		} else if (this.areaCode != pn.getAreaCode()) {
			return false;
		} else if (this.number != pn.getNumber()) {
			return false;
		}
		
		return true;
	}
}
