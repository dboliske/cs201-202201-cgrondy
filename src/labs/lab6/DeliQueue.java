package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class DeliQueue {

	public static ArrayList<String> addCustomer(ArrayList<String> data, Scanner input) {
		System.out.print("Enter a customer's name to add them to the queue: ");
		String toAdd = input.nextLine();
		
		data.add(toAdd); // automatically adds to bottom of arrayList
		
		System.out.println(toAdd + " has been added to the queue and is in position " + data.size() + ".");
		
		return data;
	}
	
	public static ArrayList<String> helpCustomer(ArrayList<String> data, Scanner input) {

		if (data.size() > 0) {
			System.out.println("Now serving: " + data.get(0)); // unnecessary to create a toRemove arrayList since you can only help one customer at a time
			data.remove(data.get(0));	
		} else { // error if queue is empty w/o this
			System.out.println("The queue is currently empty.  There is no customer to help.");
		}
		
		return data;
	}
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		ArrayList<String> data = new ArrayList<String>();
		boolean done = false;
		
		do {
			System.out.println("1. Add customer to queue");
			System.out.println("2. Help Customer");
			System.out.println("3. Exit");
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
			case "1": // add to queue
				data = addCustomer(data, input);
				break;
			case "2": // remove customer from queue
				data = helpCustomer(data, input);
				break;
			case "3": // Exit
				done = true;
				System.out.println("Shutting down...");
				break;
			default:
				System.out.println("Invalid selection.  Please try again.");
			}
		} while (!done);
		
		input.close();
	}

}
