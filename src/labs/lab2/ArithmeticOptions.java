package labs.lab2;

import java.util.Scanner;

public class ArithmeticOptions {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // read user input
		
		boolean end = false; // loop condition declared
		
		while (!end) {
			System.out.println("1. Say Hello");
			System.out.println("2. Addition");
			System.out.println("3. Multiplication");
			System.out.println("4. Exit");
			System.out.print("Choice: ");
			String option = input.nextLine(); // stores users choice
			
			switch (option) {
			case "1":
				System.out.println("Hello");
				break;
			case "2":
				System.out.print("Enter the first number: ");
				double x = Double.parseDouble(input.nextLine()); // Stores input
				System.out.print("Enter the second number: ");
				double y = Double.parseDouble(input.nextLine()); // Stores input
				System.out.println(x + " + " + y + " = " + (x+y));
				break;
			case "3":
				System.out.print("Enter the first number: ");
				double a = Double.parseDouble(input.nextLine()); // Stores input
				System.out.print("Enter the second number: ");
				double b = Double.parseDouble(input.nextLine()); // Stores input
				System.out.println(a + " * " + b + " = " + (a*b));
				break;
			case "4":
				end = true; // switches loop condition off
				break;
				default:
					System.out.println("That is not a valid choice.  Pick among the 4 numbered options."); 
			}
		}
		
		input.close();
		
		System.out.println("Shutting down...");
	}

}
