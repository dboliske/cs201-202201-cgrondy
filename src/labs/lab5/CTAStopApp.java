package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CTAStopApp {

	public static void main(String[] args) { // read in data
		Scanner input = new Scanner(System.in);
		CTAStation [] stations = readFile("src/labs/lab5/CTAStops.csv");
		stations = menu(input, stations);
		input.close();
		System.out.println("Shutting down...");
	}
	
	public static CTAStation[] readFile(String filename) { // reads file data and stores in array
		CTAStation[] stations = new CTAStation[34];
		int count = 0;
		
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			
			// end-of-file loop
			while (input.hasNextLine()) {
				try {
					String line = input.nextLine();
					String [] stops = line.split(",");
					CTAStation a = null;
					a = new CTAStation(
							stops[0],
							Double.parseDouble(stops[1]),
							Double.parseDouble(stops[2]),
							stops[3],
							Boolean.parseBoolean(stops[4]),
							Boolean.parseBoolean(stops[5])
							);
					stations[count] = a;
					count++;
				} catch (Exception e) {
					
				}
			}
			
			input.close();
		} catch (FileNotFoundException fnf) {
			// Do nothing
		} catch (Exception e) {
			System.out.println("Error occurred reading in file.");
		}
		
		return stations;
	}
	
	public static CTAStation[] menu(Scanner input, CTAStation[] stations) { // displays and performs listed options
		boolean done = false;
		
		do {
			System.out.println("1. Display Station Names");
			System.out.println("2. Display Stations with/without Wheelchair Access");
			System.out.println("3. Display Nearest Station");
			System.out.println("4. Exit");
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
			case "1":
				displayStationNames(stations);
				break;
			case "2":
				displayByWheelchair(stations, input);
				break;
			case "3":
				displayNearest(stations, input);
				break;
			case "4": 
				done = true;
				break;
			default:
				System.out.println("Invalid selection.  Please try again.");
			}
		} while (!done);
		
		return stations;
	}
	
	public static void displayStationNames(CTAStation[] stations) { // prints the name of each station
		for (int i=0; i<stations.length; i++) {
			System.out.println(stations[i].getName());
		}
	}
	
	public static void displayByWheelchair(CTAStation[] stations, Scanner input) { // checks each station for wheelchair accessibility
		int i = 0;
		int j = 0;
		String yn = "x";
		boolean result = false;
		do {
			System.out.print("Would you like to see which stops are accessible by wheelchair? Type 'y' for yes.  Type 'n' for no.");
			yn = input.nextLine();
			switch(yn.toLowerCase()) {
				case "y":
					result = true;
					break;
				case "n":
					result = false;
					break;
				default:
					System.out.println("Invalid input.  Please choose 'y' or 'n'.");
			}
		} while (!(yn.equals("y") || yn.equals("n")));
		
		if (result == true) {
			for (i=0; i<stations.length; i++) {
				if (stations[i].hasWheelchair() == true) {
				System.out.println(stations[i].getName());
			} else { // checks to see if the amount of stations w/o wheelchair accessibility is equal to the total amount of stations
				j++;
			}
			j--; // ob1 error
			if (i == j) {
				System.out.println("None of the stations are wheelchair accessible.");
			}
		}
		
		}
		
	}
	
	public static void displayNearest(CTAStation[] stations, Scanner input) { // turns input lat and lng into calculation for calcDistance(lat, lng)
		double lat1 = -200;
		double lng1 = -200;
		try {
		System.out.print("Enter your latitude.  Make sure the number is between -90 and 90: ");
		lat1 = Double.parseDouble(input.nextLine());

		System.out.print("Enter your longitude.  Make sure the number is between -180 and 180: ");
		lng1 = Double.parseDouble(input.nextLine());
		if ((lat1 >= -90 && lat1 <= 90) && (lng1 >= -180 && lng1 <= 180)) {
			int m =0;
			for (int i=1; i<stations.length; i++) {
				if (stations[i].calcDistance(lat1, lng1) < stations[m].calcDistance(lat1, lng1)) {
					m = i;
				}
			}
			
			System.out.println("the station closes to you is " + stations[m].getName());
		} else {
			System.out.println("Invalid latitude or longitude entered.  Returning to the menu.");
		}
		} catch (Exception e) {
			System.out.println("Invalid latitude or longitude entered.  Returning to the menu.");
		}

	}

}
