package labs.lab5;

public class GeoLocation {
	private double lat;
	private double lng;
	
	public GeoLocation() { 
		lat = 0.0;
		lng = 0.0;
	}
	
	public GeoLocation(double lat, double lng) { 
		this.lat = 0.0;
		setLat(lat);
		this.lng = 0.0;
		setLng(lng);
	}
	
	public double getLat() {
		return lat;
	}
	
	public double getLng() { 
		return lng;
	}
	
	public void setLat(double lat) { 
		if (lat >= -90 && lat <=90) {
			this.lat = lat;
		}
	}
	
	public void setLng(double lng) { 
		if (lng >= -180 && lng <=180) {
			this.lng = lng;
		}
	}
	
	public String toString() { 
		return "Latitude: " + lat + ". Longitude: " + lng + ".";
	}
	
	public boolean validLat(double lat) { 
		if (lat < -90 || lat > 90) {
			return false;
		}
		
		return true; // boolean cannot be conditional, so it has to be declared outside if
	}
	
	public boolean validLng(double lng) { 
		if (lng < -180 || lng > 180) {
			return false;
		}
		
		return true;
	}
	
	public boolean equals(GeoLocation g) {
		if (this.lat != g.getLat()) {
			return false;
		} else if (this.lng != g.getLng()) {
			return false;
		}
		
		return true;
	}
	
	public double calcDistance(GeoLocation g) {
		return Math.sqrt(Math.pow(lat - g.getLat(), 2) + Math.pow(lng - g.getLng(), 2)); // lat = lat1, g.getLat = lat2, lng = lng1, g.getLng = lng2
	}
	
	public double calcDistance(double lat, double lng) {
		return Math.sqrt(Math.pow(this.lat - lat, 2) + Math.pow(this.lng - lng, 2));
	}
}
