// Cole Grondy, 4/29/22
// This is the superclass.  It is inherited by Food.java and Medicine.java
package project;

public class StoreStock {
	
	private String name;
	private double price;
	private int quantity; // universal identifiers
	
	// default constructor
	public StoreStock() {
		this.name = "Apple";
		this.price = 1.00;
		this.quantity = 1;
	}
	
	// non-default constructor, takes in instance variables seen in all items
	public StoreStock(String name, double price, int quantity) {
		this.name = name;
		this.price = 1.00;
		setPrice(price);
		this.quantity = 1;
		setQuantity(quantity);
	}

	// getter for item name
	public String getName() {
		return name;
	}

	// setter for item name
	public void setName(String name) {
		this.name = name;
	}

	// getter for item price
	public double getPrice() {
		return price;
	}

	// setter for item price
	public void setPrice(double price) {
		if (price > 0) {
			this.price = price;
		}
	}

	// getter for item quantity
	public int getQuantity() {
		return quantity;
	}

	// setter for item quantity
	public void setQuantity(int quantity) {
		if (quantity > 0) {
			this.quantity = quantity;
		}
	}
	
	// overrides equals function in Object.  
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof StoreStock)) {
			return false;
		}
		
		StoreStock s = (StoreStock)obj;
		if (!this.name.contentEquals(s.getName())) {
			return false;
		} else if (this.price != s.getPrice()) {
			return false;
		} else if (this.quantity != s.getQuantity()) {
			return false;
		}
		
		return true;
	}
	
	// overrides toString function in Object
	@Override
	public String toString() {
		return "Item: " + name + ". Price: " + price + ". Quantity: " + quantity + ". ";
	}
	
	// used by toCSV below
	protected String csvData() {
		return name + "," + price + "," + quantity;
	}
	
	// part of what gets written to files.  Used by Food.java and Medicine.java
	public String toCSV() {
		return "StoreStock," + csvData();
	}
	
	

}
