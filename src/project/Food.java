// Cole Grondy, 4/29/22
// This is a subclass that handles food items.  It inherits from StoreStock.java
package project;

public class Food extends StoreStock {
	
	private boolean perishable; // food identifier
	
	// default constructor
	public Food() {
		super();
		perishable = false;
	}
	
	// non-default constructor, takes in instance variables seen in all food items
	public Food(String name, double price, int quantity, boolean perishable) {
		super(name, price, quantity);
		this.perishable = perishable;
	}

	// getter for item perishability
	public boolean isPerishable() {
		return perishable;
	}
	
	// setter for item perishability
	public void setPerishable(boolean perishable) {
		this.perishable = perishable;
	}
	
	// overrides equals method in StoreStock
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof Food)) {
			return false;
		}
		
		Food f = (Food)obj;
		if (this.perishable != f.isPerishable()) {
			return false;
		}
		
		return true;
	}
	
	// overrides toString in StoreStock.java
	// when more data about a food is requested, this is the method called
	@Override
	public String toString() {
		return super.toString() + "Category: Food. Perishable: " + perishable + ".";
	}
	
	// overrides toCSV in StoreStock.java 
	// when an added item is a food, this is what goes to StoreStock.csv
	@Override
	public String toCSV() {
		return "Food," + super.csvData() + "," + perishable;
	}

}
