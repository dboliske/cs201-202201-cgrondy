// Cole Grondy, 4/29/22
// This is a subclass that handles medicine items.  It inherits from StoreStock.java
package project;

public class Medicine extends StoreStock {
	
	private boolean prescription; // medicine identifier
	
	// default constructor
	public Medicine() {
		super();
		prescription = false;
	}
	
	// non-default constructor, takes in instance variables seen in all medicine items
	public Medicine(String name, double price, int quantity, boolean prescription) {
		super(name, price, quantity);
		this.prescription = prescription;
	}

	// getter for prescription requirement
	public boolean isprescription() {
		return prescription;
	}

	// setter for prescription requirement
	public void setprescription(boolean prescription) {
		this.prescription = prescription;
	}
	
	// overrides equals method in StoreStock
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof Medicine)) {
			return false;
		}
		
		Medicine m = (Medicine)obj;
		if (this.prescription != m.isprescription()) {
			return false;
		}
		
		return true;
	}
	
	// overrides toString in StoreStock.java
	// when more data about a medicine is requested, this is the method called
	@Override
	public String toString() {
		return super.toString() + "Category: Medicine. prescription Required: " + prescription + ".";
	}
	
	// overrides toCSV in StoreStock.java 
	// when an added item is a medicine, this is what goes to StoreStock.csv
	@Override
	public String toCSV() {
		return "Medicine," + super.csvData() + "," + prescription;
	}


}
