// Cole Grondy, 4/29/22
// This is the application class.  This has the main method from which everything is run, pulls data from StockInventory.csv, and uses StoreStock.java functions
package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class StoreStockApp {
	
	// method from which all other user input methods are accessed (except initial file load
	public static StoreStock[] menu(Scanner input, StoreStock[] data) {
		boolean done = false;
		
		do {
			System.out.println("1. Add Items to Stock");
			System.out.println("2. Serve Customer");
			System.out.println("3. Search Stock");
			System.out.println("4. Modify Stock");
			System.out.println("5. Create a New Stock");
			System.out.println("6. Exit");
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
			case "1": // add item to stock
				data = addItem(data, input);
				break;
			case "2": // add to cart, remove cart from stock
				data = serveCustomer(data, input);
				break;
			case "3": // search the stock
				data = searchStock(data, input);
				break;
			case "4": // change stock items
				data = modifyStock(data, input);
				break;
			case "5": // creates a new file
				newFile(input, data);
				break;
			case "6": // Exit
				done = true;
				break;
			default:
				System.out.println("Invalid selection.  Please try again.");
			}
		} while (!done);
		
		return data;
	}
	
	// adds new line with input data into the file.  Stock does not have to be sorted because the search type is sequential
	public static StoreStock[] addItem(StoreStock[] data, Scanner input) {
		
		StoreStock s;
		System.out.print("Item Name: ");
		String name = input.nextLine();
		System.out.print("Item Price: ");
		double price = 0;
		try {
			price = Double.parseDouble(input.nextLine());
			if (price <= 0) {
				System.out.println("Invalid price.  Starting over...");
				return data;
			}
		} catch (Exception e) {
			System.out.println("Invalid price.  Starting over...");
			return data;
		}
		System.out.print("Item Quantity: ");
		int quantity = 0;
		try {
			quantity = Integer.parseInt(input.nextLine());
			if (quantity <= 0) {
				System.out.println("Invalid quantity.  Starting over...");
				return data;	
			}
		} catch (Exception e) {
			System.out.println("Invalid quantity.  Starting over...");
			return data;
		}
		
		System.out.println("Item Category (Food or Medicine): ");
		String type = input.nextLine();
		switch (type.toLowerCase()) { // this is where the difference of either food or medicine class is called
		case "food":
			System.out.print("Perishable (y/n): ");
			boolean perishable = false;
			String yn = input.nextLine();
			switch(yn.toLowerCase()) {
				case "yes":
				case "y":
					perishable = true;
					break;
				case "no":
				case "n":
					perishable = false;
					break;
				default:
					System.out.println("Invalid input.  Starting over...");
					return data;
			}
			s = new Food(name, price, quantity, perishable);
			break;
		case "medicine":
			System.out.print("Requires prescription (y/n): ");
			boolean prescription = false;
			String yesno = input.nextLine();
			switch(yesno.toLowerCase()) {
				case "yes":
				case "y":
					prescription = true;
					break;
				case "no":
				case "n":
					prescription = false;
					break;
				default:
					System.out.println("Invalid input.  Starting over...");
					return data;
			}
			s = new Medicine(name, price, quantity, prescription);
			break;
		default: 
			System.out.println("That is not a valid category.  Returning to menu.");
			return data;
		}
		
		data = resize(data, data.length+1);
		data[data.length - 1] = s;
		
		return data;
	}
	
	// gives user choice to increase cart size or remove cart entries from stock
	public static StoreStock[] serveCustomer(StoreStock[] data, Scanner input) {
		
		boolean done = false;
		ArrayList<String> cart = new ArrayList<String>();

		do {
			System.out.println("1. Add item to cart");
			System.out.println("2. Check out");
			System.out.println("3. Exit");
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
			case "1": // add item to cart
				cart = addToCart(cart, data, input);
				break;
			case "2": // remove cart items from stock
				data = checkOut(data, cart, input);
				break;
			case "3": // Exit
				done = true;
				break;
			default:
				System.out.println("Invalid selection.  Please try again.");
			}
			
		} while (!done);
		return data;
		
	}
	
	// adds stock items to an arrayList to keep track of everything to be removed from the stock
	public static ArrayList<String> addToCart(ArrayList<String> cart, StoreStock[] data, Scanner input) {
		
		System.out.print("Choose an item to add to the cart: ");
		String value = input.nextLine();
		int index = sequentialSearch(data, value);
		if (index == -1) {
			System.out.println(value + " not found.");
		} else {
			System.out.print("How many are you adding?: ");
			int quantity = 0;
			try {
				quantity = Integer.parseInt(input.nextLine());
				if (quantity <= 0) {
					System.out.println("Invalid amount.  Starting over...");
					return cart;	
				}
			}  catch (Exception e) {
				System.out.println("Invalid amount.  Starting over...");
				return cart;
			}
			if ((data[index].getQuantity()) < quantity) {
				System.out.println("There are not enough of this item in stock.  Please try again.");
			} else {
				cart.add(value + "," + quantity); // splits each arrayList entry into the name and quantity so that we can use the data to compare and manipulate the stock with proper names
			}
		}
		
		return cart;
	}
	
	// lists all cart items, searches each arrayList name for the matching stock item, subtracts the cart quantity from the stock quantity.  Removes stock entry if quantity is 0
	public static StoreStock[] checkOut(StoreStock[] data, ArrayList<String> cart, Scanner input) {
		
		System.out.println(cart);
		System.out.print("This is everything in your cart.  Are you ready to finish? (y/n): ");
		
		String yn = input.nextLine();
		switch(yn.toLowerCase()) {
			case "yes":
			case "y":
				for (int i=0; i<cart.size(); i++) {
					String line = cart.get(i);
					String[] cartValue = line.split(",");
					int index = sequentialSearch(data, cartValue[0]);
					String stockLine = data[index].toCSV();
					String[] stockValue = stockLine.split(",");
					String type = stockValue[0];
					String name = stockValue[1];
					Double price = Double.parseDouble(stockValue[2]);
					int quantity = (Integer.parseInt(stockValue[3]))-(Integer.parseInt(cartValue[1]));
					boolean perishable = Boolean.parseBoolean(stockValue[4]);
					boolean prescription = Boolean.parseBoolean(stockValue[4]); // perishable and prescription can pull from the same position since both item types have the same amount of data
					switch (type.toLowerCase()) {  // this is where the difference of either food or medicine class is called
					case "food":
						data[index] = new Food(name, price, quantity, perishable);
						break;
					case "medicine":
						data[index] = new Medicine(name, price, quantity, prescription);
						break;
					default: 
						System.out.println("That is not a valid category.  Returning to menu.");
						return data;
					}
					if (quantity == 0) { // checks if all instances of an item are ordered
						data = removeItem(data, index);
					}
				}
				cart.clear();
				break;
			case "no":
			case "n":
				return data;
			default:
				System.out.println("Invalid input.  Starting over...");
				return data;
		}
		return data;
	}
	
	// most basic means of calling the sequentialSearch method, spits out the toString
	public static StoreStock[] searchStock(StoreStock[] data, Scanner input) {
		
		System.out.print("enter the name of an item: ");
		String value = input.nextLine();
		int index = sequentialSearch(data, value);
		if (index == -1) {
			System.out.println(value + " not found.");
		} else {
			System.out.println(data[index].toString());
		}
		
		return data;
		
	}
	
	// searches the stock and prompts for data to be changed on that line.  Replaces entire line with a new food or medicine
	public static StoreStock[] modifyStock(StoreStock[] data, Scanner input) {
		
		System.out.print("enter the name of an item: ");
		String value = input.nextLine();
		int index = sequentialSearch(data, value);
		if (index == -1) {
			System.out.println(value + " not found.");
		} else {
				String name = value;
				System.out.print("New Price: ");
				double price = 0;
				try {
					price = Double.parseDouble(input.nextLine());
					if (price <= 0) {
						System.out.println("Invalid price.  Starting over...");
						return data;
					}
				} catch (Exception e) {
					System.out.println("Invalid price.  Starting over...");
					return data;
				}
				System.out.print("New Quantity: ");
				int quantity = 0;
				try {
					quantity = Integer.parseInt(input.nextLine());
					if (quantity <= 0) {
						System.out.println("Invalid quantity.  Starting over...");
						return data;	
					}
				} catch (Exception e) {
					System.out.println("Invalid quantity.  Starting over...");
					return data;
				}
				
				System.out.println("Item Category (Food or Medicine): ");
				String type = input.nextLine();
				switch (type.toLowerCase()) {
				case "food":
					System.out.print("Perishable (y/n): ");
					boolean perishable = false;
					String yn = input.nextLine();
					switch(yn.toLowerCase()) {
						case "yes":
						case "y":
							perishable = true;
							break;
						case "no":
						case "n":
							perishable = false;
							break;
						default:
							System.out.println("Invalid input.  Starting over...");
							return data;
					}
					data[index] = new Food(name, price, quantity, perishable);
					break;
				case "medicine":
					System.out.print("Requires prescription (y/n): ");
					boolean prescription = false;
					String yesno = input.nextLine();
					switch(yesno.toLowerCase()) {
						case "yes":
						case "y":
							prescription = true;
							break;
						case "no":
						case "n":
							prescription = false;
							break;
						default:
							System.out.println("Invalid input.  Starting over...");
							return data;
					}
					data[index] = new Medicine(name, price, quantity, prescription);
					break;
				default: 
					System.out.println("That is not a valid category.  Returning to menu.");
					return data;
				}
				
				return data;
		}
		
		return data;
		
	}
	
	// creates a new file containing the current stock
	public static void newFile(Scanner input, StoreStock[] data) {
		System.out.println("Enter a name for the new file. This should include the filepath and the file type (i.e. 'src/project/StoreStockOld.csv').");
		System.out.print("Filename: ");
		String filename = input.nextLine();
		saveFile(filename, data);
	}
	
	// every method that needs an existing stock entry uses this to find it.  Sequential search is used so sorting the stock is unnecessary
	public static int sequentialSearch(StoreStock[] data, String value) {
		for (int i=0; i<data.length; i++) {
			if ((data[i].getName()).equalsIgnoreCase(value)) {
				return i;
			}
		}
		
		return -1;
	}
	
	// this method is given a specific line of the stock to remove, and adds every other line to the new array
	public static StoreStock[] removeItem(StoreStock[] data, int index) {
		StoreStock[] temp = new StoreStock[data.length - 1];
		
		for (int i=0, j=0; i < data.length; i++) {
			if (i != index) {
				temp[j++] = data[i]; 
			}
		}
		
		return temp;
	}
	
	// this method is used whenever an entry is added to the stock.  Increases the array size to accomodate for more entries
	public static StoreStock[] resize(StoreStock[] data, int size) {
		StoreStock[] temp = new StoreStock[size];
		int limit = data.length > size ? size : data.length;
		for (int i=0; i<limit; i++) {
			temp[i] = data[i];
		}
		
		return temp;
	}
	
	// adds each line to the main StoreStock array and separates each line by its data type.  This allows getters, setters, and identifiers to handle the correct data properly
	public static StoreStock[] readFile(String filename) {
		StoreStock[] stock = new StoreStock[10];
		int count = 0;
		
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			
			// end-of-file loop
			while (input.hasNextLine()) {
				try {
					String line = input.nextLine();
					String [] values = line.split(",");
					StoreStock a = null;
					switch (values[0].toLowerCase()) { // the order as it appears in the file is type,name,price,quantity,perishable/prescription
						case "food":
							a = new Food(
									values[1],
									Double.parseDouble(values[2]),
									Integer.parseInt(values[3]),
									Boolean.parseBoolean(values[4])
									);
							break;
						case "medicine":
							a = new Medicine(
									values[1],
									Double.parseDouble(values[2]),
									Integer.parseInt(values[3]),
									Boolean.parseBoolean(values[4])
									);
							break;
					}
					if (stock.length == count) {
					stock = resize(stock, stock.length*2);
					}
					
					stock[count] = a;
					count++;
				} catch (Exception e) {
					
				}
			}
			
			input.close();
		} catch (FileNotFoundException fnf) {
			// Do nothing
		} catch (Exception e) {
			System.out.println("Error occurred reading in file.");
		}
		
		stock = resize(stock, count);
		
		return stock;
	}
	
	// updates currently loaded file and creates new files when called.
	public static void saveFile(String filename, StoreStock[] data) {
		try {
			FileWriter writer = new FileWriter(filename);
			
			for (int i=0; i<data.length; i++) {
				writer.write(data[i].toCSV() + "\n");
				writer.flush();
			}
			
			writer.close();
		} catch (Exception e) {
			System.out.println("Error trying to save file.");
		}
	}

	// loads and saves desired file, calls the menu
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Load file: "); // pathname is "src/project/StockInventory.csv"
		String filename = input.nextLine();
		StoreStock[] data = readFile(filename);
		data = menu(input, data);
		saveFile(filename, data);
		input.close();
		System.out.println("Shutting down...");
		

	}

}
